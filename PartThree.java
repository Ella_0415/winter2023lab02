import java.util.Scanner;
public class PartThree
{
	public static void main(String[] args)
	{
		Scanner num = new Scanner(System.in);
		System.out.println("Please choose a first number: ");
		int n1 = num.nextInt();
		System.out.println("Please choose a second number: ");
		int n2 = num.nextInt();
		
		System.out.println("Sum of the two numbers: ");
		System.out.println(Calculator.add(n1, n2));
		
		System.out.println("Subtraction of the two numbers: ");
		System.out.println(Calculator.subtract(n1,n2));
		
		Calculator cal = new Calculator();
		
		System.out.println("Multiplication of the two numbers: ");
		System.out.println(cal.multiply(n1,n2));
		
		System.out.println("Division of the two numbers: ");
		System.out.println(cal.divide(n1,n2));
	}
}