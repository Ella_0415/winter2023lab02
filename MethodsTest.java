public class MethodsTest
{
	public static void main(String[] args)
	{
		int x = 5;
		
		System.out.println(x);
		methodNoInputNoReturn();
		
		System.out.println(x);
		
		methodOneInputNoReturn(x + 10);
		
		System.out.println(x);
		
		methodTwoInputNoReturn(3, 4.5);
		
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		double root2 = sumSquareRoot(9,5);
		System.out.println(root2);
		
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn()
	{
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int integer)
	{
		System.out.println("Inside the method one input no return");
		integer = integer - 5;
		System.out.println(integer);
	}
	
	public static void methodTwoInputNoReturn(int value1, double value2)
	{
		System.out.println(value1);
		System.out.println(value2);
	}
	
	public static int methodNoInputReturnInt()
	{
		return 5;
	}
	
	public static double sumSquareRoot(int integ, int integ2)
	{
		double root = integ + integ2;
		root = Math.sqrt(root);
		return root;
	}
}