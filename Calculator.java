public class Calculator
{
	public static int add(int num1, int num2)
	{
		int addition = num1 + num2;
		return addition;
	}
	
	public static int subtract(int num3, int num4)
	{
		int subtraction = num3 - num4;
		return subtraction;
	}
	
	public double multiply(int num5, int num6)
	{
		double multiplication = num5 * num6;
		return multiplication;
	}
	
	public double divide(int num7, int num8)
	{
		double division = num7/num8;
		return division;
	}
}